# Copyright 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Implements a phrase lookup index that uses full text phrases and (optionally)
# their singular and plural variants.


import codecs
from pattern.en import pluralize, singularize
from baseindex import BaseIndex


class PhraseIndex(BaseIndex):
    def __init__(self):
        BaseIndex.__init__(self)

        # A set to contain the full candidate phrases as loaded from the source
        # files.
        self.source_phrases = set()

        # A dictionary that maps singular/plural variants to source phrases.
        self.variant_phrases = {}

        # Whether to generate plural/singular variants of phrases.
        self.gen_variants = True

        self.c_wordcnt = 0
        self.c_phrasecnt = 0

    def getGenerateVariants(self):
        return self.gen_variants

    def setGenerateVariants(self, newval):
        self.gen_variants = newval

    def getCorpusWordCnt(self):
        return self.c_wordcnt
    
    def getCorpusPhraseCnt(self):
        return self.c_phrasecnt

    def getSourcePhraseCount(self):
        """
        Returns the total number of source phrases in the index.
        """
        return len(self.source_phrases)

    def getVariantPhraseCount(self):
        """
        Returns the total count of variant phrases that are not also included
        in the main phrase index.
        """
        pcnt = 0
        for phrase in self.variant_phrases:
            if phrase not in self.source_phrases:
                pcnt += 1

        return pcnt

    def _addVariant(self, variantphrase, sourcephrase):
        """
        Adds an expanded (singular/plural variant) phrase to the expanded
        phrases dictionary, and maps the variant to the source phrase.
        """
        if variantphrase not in self.source_phrases:
            if variantphrase not in self.variant_phrases:
                self.variant_phrases[variantphrase] = set()

            self.variant_phrases[variantphrase].add(sourcephrase)

    def _addPhraseInternal(self, phrase):
        """
        Adds a single phrase (and, optionally, its plural/singular variants) to
        the index.
        """
        phrase = phrase.lower()
        phrase = self._depunctuate(phrase)
    
        if phrase != '':
            self.source_phrases.add(phrase)

            pwords = phrase.split()
            psize = len(pwords)
            if psize > self.max_phrasesize:
                self.max_phrasesize = psize
    
            if self.gen_variants:
                # Generate plural and singular forms.
                singular = (' '.join(
                    pwords[0:psize-1] + [singularize(pwords[psize - 1])]
                ))
                self._addVariant(singular, phrase)
    
                plural = (' '.join(
                    pwords[0:psize-1] + [pluralize(pwords[psize - 1])]
                ))
                self._addVariant(plural, phrase)

    def lookupPhrase(self, testphrase):
        """
        Looks up a test phrase to see if it matches one of the candidate
        phrases or its singular/plural variants.  If a match is found, returns
        a list containing the matching phrases.  If not, returns an empty list.
        Note that the phrase to search for, testphrase, should be lower case
        and de-punctuated.
        """
        if testphrase in self.source_phrases:
            return [testphrase]
        elif testphrase in self.variant_phrases:
            phrases = list(self.variant_phrases[testphrase])
            phrases.sort()

            return phrases
        else:
            return []

    def lookupSentence(self, sentence):
        """
        Looks for keyphrase matches among all possible phrases (up to the
        maximum candidate phrase length) in a sentence of plain text.  Returns
        a dictionary mapping matching phrases to the number of times they were
        found in the sentence.
        """
        sentence = sentence.lower()
        sentence = self._depunctuate(sentence)
    
        matches = {}

        words = sentence.split()
        self.c_wordcnt += len(words)
    
        # Examine all phrases in the sentence of length up to the maximum
        # phrase length found in the index.
        for phrase in self._getSentencePhrases(words):
            self.c_phrasecnt += 1

            res = self.lookupPhrase(phrase)

            if len(res) > 0:
                mphrases = ', '.join(res)
                if mphrases not in matches:
                    matches[mphrases] = 1
                else:
                    matches[mphrases] += 1

        return matches

