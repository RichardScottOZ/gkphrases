# Copyright 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Implements a phrase lookup index that uses full text phrases and (optionally)
# their singular and plural variants.


from nltk.stem import PorterStemmer
from baseindex import BaseIndex


class StemIndex(BaseIndex):
    def __init__(self):
        BaseIndex.__init__(self)

        # A dictionary that maps stemmed phrases to full phrases.
        self.stem_phrases = {}

        # The maximum phrase size, in words.
        self.max_phrasesize = 0

        self.pstemmer = PorterStemmer()

        self.c_wordcnt = 0
        self.c_phrasecnt = 0

    def getCorpusWordCnt(self):
        return self.c_wordcnt
    
    def getCorpusPhraseCnt(self):
        return self.c_phrasecnt

    def getStemmedPhraseCount(self):
        """
        Returns the total count of unique stemmed phrases in the index.
        """
        return len(self.stem_phrases)

    def getSourcePhraseCount(self):
        """
        Returns the total number of unique source phrases in the index.
        """
        srcphrases = set()

        for stemphrase in self.stem_phrases:
            srcphrases.update(self.stem_phrases[stemphrase])

        return len(srcphrases)

    def _addPhraseInternal(self, phrase):
        """
        Adds a single phrase to the index.
        """
        phrase = phrase.lower()
        phrase = self._depunctuate(phrase)
    
        if phrase != '':
            pwords = phrase.split()
            psize = len(pwords)
            if psize > self.max_phrasesize:
                self.max_phrasesize = psize
    
            stems = []
            for pword in pwords:
                stems.append(self.pstemmer.stem(pword))
            stemphrase = ' '.join(stems)

            if stemphrase not in self.stem_phrases:
                self.stem_phrases[stemphrase] = set()

            self.stem_phrases[stemphrase].add(phrase)

    def lookupSentence(self, sentence):
        """
        Looks for keyphrase matches among all possible phrases (up to the
        maximum candidate phrase length) in a sentence of plain text.  Returns
        a dictionary mapping matching phrases to the number of times they were
        found in the sentence.
        """
        sentence = sentence.lower()
        sentence = self._depunctuate(sentence)
    
        rwords = sentence.split()
        self.c_wordcnt += len(rwords)

        # Convert the sentence to a stemmed sentence.
        swords = []
        for rword in rwords:
            swords.append(self.pstemmer.stem(rword))
    
        matches = {}
    
        # Examine all phrases in the sentence of length up to the maximum
        # phrase length found in the index.
        for phrase in self._getSentencePhrases(swords):
            self.c_phrasecnt += 1

            if phrase in self.stem_phrases:
                phraselist = list(self.stem_phrases[phrase])
                phraselist.sort()
                mphrases = ', '.join(phraselist)
                if mphrases not in matches:
                    matches[mphrases] = 1
                else:
                    matches[mphrases] += 1

        return matches

