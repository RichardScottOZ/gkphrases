# Copyright 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from abc import ABCMeta, abstractmethod
import string, re
import requests
import time


# Define the standard and typographic apostrophe characters.
apost_chars = ["'", unichr(8217)]

# Add typographic double quotes to the set of punctuation characters.
punctchars = string.punctuation + unichr(8220) + unichr(8221) + unichr(8217)
punct_re = re.compile('[' + punctchars + ']')

# The URL for the Catalog of Life web service end point.
COL_URL = 'http://webservice.catalogueoflife.org/col/webservice'


class BaseIndex:
    """
    An abstract base class for all keyphrase indexing classes.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        # The maximum phrase size, in words.
        self.max_phrasesize = 0

        # Whether to filter taxonomic names from the set of candidate phrases.
        self.filter_taxa = False

        # A dictionary to cache the results of CoL lookups so we can avoid
        # redundant web service calls.  For each name, we record the result
        # that should be returned by _CoLLookup(). 
        self.col_cache = {}

    def getFilterTaxonomicNames(self):
        return self.filter_taxa

    def setFilterTaxonomicNames(self, newval):
        self.filter_taxa = newval

    def _depunctuate(self, text):
        """
        "De-punctuates" a sentence or phrase by removing all punctuation and
        possessive forms.
        """
        # Remove possessive forms.
        for apost_char in apost_chars:
            text = text.replace(apost_char + 's ', ' ')
            text = text.replace(apost_char + ' ', ' ')
    
        # Remove remaining punctuation.
        text = punct_re.sub('', text)
            
        return text

    def _getSentencePhrases(self, words):
        """
        Returns a list of all possible phrases (up to the maximum candidate
        phrase length) in a sentence of plain text.  The sentence should be
        provided as a list of words, in sentence order.
        """
        phrases = []

        # Generate all phrases in the sentence of length up to the maximum
        # phrase length found in the index.
        curword = 0
        while curword < len(words):
            phraselen = 1
            while (
                phraselen <= self.max_phrasesize and
                (curword + phraselen) <= len(words)
            ):
                curphrase = ' '.join(words[curword:curword+phraselen])
                phrases.append(curphrase)
    
                phraselen += 1
    
            curword += 1

        return phrases

    def _CoLLookup(self, namestr):
        """
        Looks up a (possible) name string in Catalog of Life.  Returns True if
        the name is a known taxonomic name; returns False otherwise.
        """
        if namestr in self.col_cache:
            return self.col_cache[namestr]

        retry_max = 4
        retry_cnt = 0
        succeeded = False

        while not(succeeded) and (retry_cnt < retry_max):
            try:
                res = requests.get(
                    COL_URL,
                    {'name': namestr, 'format': 'json', 'response': 'terse'}
                )
                succeeded = True
            except requests.exceptions.ConnectionError as err:
                retry_cnt += 1
                if retry_cnt >= retry_max:
                    print ('ERROR: Encountered connection error when '
                    'attempting to search for "{0}" in Catalog of Life.  '
                    'Maximum retries ({1}) exceeded.  The error message '
                    'was: "{2}"'.format(namestr, retry_max, err.message))
                    return False
                else:
                    print ('Warning: Encountered connection error when '
                    'attempting to search for "{0}" in Catalog of Life.  '
                    'Retrying in 4 seconds...'.format(namestr))
                    time.sleep(4)

        dstruct = res.json()

        retval = False

        if dstruct['total_number_of_results'] > 0:
            # If we got matches, make sure we got an exact match (CoL will
            # return partial matches).
            for record in dstruct['results']:
                if record['name'] == namestr:
                    retval = True

        self.col_cache[namestr] = retval

        return retval

    def getMaxPhraseSize(self):
        """
        Returns the maximum source phrase size, in words.
        """
        return self.max_phrasesize

    @abstractmethod
    def getSourcePhraseCount(self):
        """
        Returns the total number of candidate source phrases in the index.
        """
        pass

    @abstractmethod
    def getCorpusWordCnt(self):
        """
        Returns the total number of words in the search text that were
        inspected for phrase matching.
        """
        pass
    
    @abstractmethod
    def getCorpusPhraseCnt(self):
        """
        Returns the total number of phrases in the search text that were
        inspected for phrase matching.
        """
        pass
    
    @abstractmethod
    def _addPhraseInternal(self, phrase):
        """
        A "private" method that adds a single candidate phrase to the relevant
        internal data structures.
        """
        pass

    def addPhrase(self, phrase):
        """
        Adds a single phrase to the index.
        """
        if self.filter_taxa:
            if not(self._CoLLookup(phrase)):
                self._addPhraseInternal(phrase)
        else:
            self._addPhraseInternal(phrase)

    @abstractmethod
    def lookupSentence(self, sentence):
        """
        Looks for keyphrase matches among all possible phrases (up to the
        maximum candidate phrase length) in a sentence of plain text.  Returns
        a dictionary mapping matching phrases to the number of times they were
        found in the sentence.
        """
        pass

