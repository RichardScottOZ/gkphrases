# Copyright (C) 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gkphrases.stemindex import StemIndex
import unittest


class TestStemIndex(unittest.TestCase):
    """
    Tests the StemIndex class.
    """
    def setUp(self):
        self.si = StemIndex()

    def test_addPhraseInternal(self):
        self.assertEqual(0, self.si.getStemmedPhraseCount())
        self.assertEqual(0, self.si.getSourcePhraseCount())
        self.assertEqual(0, self.si.getMaxPhraseSize())

        self.si._addPhraseInternal('Abdomen')
        self.assertEqual(1, self.si.getStemmedPhraseCount())
        self.assertEqual(1, self.si.getSourcePhraseCount())
        self.assertEqual(1, self.si.getMaxPhraseSize())
        self.assertEqual({'abdomen': {'abdomen'}}, self.si.stem_phrases)

        self.si._addPhraseInternal('fly')
        self.assertEqual(2, self.si.getStemmedPhraseCount())
        self.assertEqual(2, self.si.getSourcePhraseCount())
        self.assertEqual(1, self.si.getMaxPhraseSize())
        self.assertEqual(
            {'abdomen': {'abdomen'}, 'fli': {'fly'}}, self.si.stem_phrases
        )

        self.si._addPhraseInternal('flies')
        self.assertEqual(2, self.si.getStemmedPhraseCount())
        self.assertEqual(3, self.si.getSourcePhraseCount())
        self.assertEqual(1, self.si.getMaxPhraseSize())
        self.assertEqual(
            {'abdomen': {'abdomen'}, 'fli': {'fly', 'flies'}},
            self.si.stem_phrases
        )

        self.si._addPhraseInternal('fly abdomens')
        self.assertEqual(3, self.si.getStemmedPhraseCount())
        self.assertEqual(4, self.si.getSourcePhraseCount())
        self.assertEqual(2, self.si.getMaxPhraseSize())
        self.assertEqual(
            {
                'abdomen': {'abdomen'}, 'fli': {'fly', 'flies'},
                'fli abdomen': {'fly abdomens'}
            },
            self.si.stem_phrases
        )

    def test_lookupSentence(self):
        self.si._addPhraseInternal('abdomen')
        self.si._addPhraseInternal('fly')
        self.si._addPhraseInternal('flies')

        self.assertEqual({}, self.si.lookupSentence('Does not match.'))
        self.assertEqual({'abdomen': 1}, self.si.lookupSentence('Abdomen.'))
        self.assertEqual(
            {'abdomen': 1},
            self.si.lookupSentence('All insects have an abdomen.')
        )
        self.assertEqual(
            {'abdomen': 1},
            self.si.lookupSentence('All insects have abdomens.')
        )
        self.assertEqual(
            {'abdomen': 2},
            self.si.lookupSentence('Abdomens: All insects have an abdomen.')
        )
        self.assertEqual(
            {'abdomen': 2, 'flies, fly': 1},
            self.si.lookupSentence(
                'Abdomens: All flies have an abdomen.'
            )
        )
        self.assertEqual(
            {'abdomen': 2, 'flies, fly': 1},
            self.si.lookupSentence(
                "Abdomens: This fly's abdomen."
            )
        )

