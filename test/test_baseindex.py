# -*- coding: utf8 -*-

# Copyright (C) 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from gkphrases.baseindex import BaseIndex
import unittest


class BaseIndexAdapter(BaseIndex):
    """
    A "dummy" class that provides no-op concrete implementations of all
    abstract methods from BaseIndex so that the concrete methods in BaseIndex
    can be tested.  Also counts calls to _addPhraseInternal().
    """
    def __init__(self):
        BaseIndex.__init__(self)

        self.addpi_cnt = 0

    def getCorpusWordCnt(self):
        pass
    
    def getCorpusPhraseCnt(self):
        pass

    def getSourcePhraseCount(self):
        pass

    def _addPhraseInternal(self, phrase):
        self.addpi_cnt += 1

    def lookupSentence(self, sentence):
        pass


class TestBaseIndex(unittest.TestCase):
    """
    Tests the concrete methods of the BaseIndex class.
    """
    def setUp(self):
        self.bi = BaseIndexAdapter()

    def test_depunctuate(self):
        self.assertEqual('fly', self.bi._depunctuate('fly'))
        self.assertEqual('fly abdomen', self.bi._depunctuate("fly's abdomen"))
        self.assertEqual('fly abdomen', self.bi._depunctuate("fly’s abdomen"))
        self.assertEqual('flies abdomen', self.bi._depunctuate("flies' abdomen"))
        self.assertEqual('flys', self.bi._depunctuate("fly's"))
        self.assertEqual('flys', self.bi._depunctuate("fly’s"))
        self.assertEqual('flies abdomen', self.bi._depunctuate("“flies” abdomen"))

    def test_getSentencePhrases(self):
        self.bi.max_phrasesize = 1

        self.assertEqual([], self.bi._getSentencePhrases([]))
        self.assertEqual(['a'], self.bi._getSentencePhrases(['a']))
        self.assertEqual(
            ['a', 'test', 'sentence'],
            self.bi._getSentencePhrases(['a', 'test', 'sentence'])
        )

        self.bi.max_phrasesize = 2
        self.assertEqual(
            ['a', 'a test', 'test', 'test sentence', 'sentence'],
            self.bi._getSentencePhrases(['a', 'test', 'sentence'])
        )

        self.bi.max_phrasesize = 3
        self.assertEqual(
            [
                'a', 'a test', 'a test sentence', 'test', 'test sentence',
                'sentence'
            ],
            self.bi._getSentencePhrases(['a', 'test', 'sentence'])
        )

    def test_CoLLookup(self):
        self.assertFalse('Diptera' in self.bi.col_cache)
        self.assertTrue(self.bi._CoLLookup('Diptera'))
        self.assertTrue('Diptera' in self.bi.col_cache)
        self.assertTrue(self.bi._CoLLookup('Diptera'))

        self.assertTrue(self.bi._CoLLookup('Sarcophagidae'))
        self.assertTrue(self.bi._CoLLookup('Emblemasoma'))
        self.assertTrue(self.bi._CoLLookup('Emblemasoma erro'))

        self.assertFalse('Nota valid name' in self.bi.col_cache)
        self.assertFalse(self.bi._CoLLookup('Nota valid name'))
        self.assertTrue('Nota valid name' in self.bi.col_cache)
        self.assertFalse(self.bi._CoLLookup('Nota valid name'))

    def test_addPhraseInternal(self):
        # Verify that taxonomic names are excluded when taxonomic name
        # filtering is turned on.
        self.bi.setFilterTaxonomicNames(True)

        self.assertEqual(0, self.bi.addpi_cnt)
        self.bi.addPhrase('Diptera')
        self.bi.addPhrase('Emblemasoma')
        self.bi.addPhrase('Emblemasoma erro')
        self.assertEqual(0, self.bi.addpi_cnt)

        self.bi.addPhrase('Nota valid name')
        self.assertEqual(1, self.bi.addpi_cnt)

        # Verify that taxonomic names are added when taxonomic name filtering
        # is turned off.
        self.bi.setFilterTaxonomicNames(False)

        self.bi.addPhrase('Diptera')
        self.bi.addPhrase('Emblemasoma')
        self.bi.addPhrase('Emblemasoma erro')
        self.assertEqual(4, self.bi.addpi_cnt)

        self.bi.addPhrase('Nota valid name')
        self.assertEqual(5, self.bi.addpi_cnt)

